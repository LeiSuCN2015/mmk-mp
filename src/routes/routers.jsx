import React from 'react';
import {Route,Router, hashHistory} from 'react-router';
import { Provider } from 'mobx-react';
import { useStrict } from 'mobx';

import Home from '../components/main/main.jsx'
import SendReveicer from '../components/send/sendReveicer.jsx'
import SendSender from '../components/send/sendSender.jsx'
import AddReceiverInfo from '../components/AddReceiverInfo/index.jsx'
import EditReceiverInfo from '../components/AddReceiverInfo/editReceiver.jsx'
import Region from '../components/Region/index.jsx'
import PickUpSender from '../components/pickupSender/index.jsx'
import PickReveicer from '../components/pickupReceiver/pickReveicer.jsx'
import AddPickUpSenderInfo from '../components/pickupReceiver/addPickUpSenderInfo.jsx'
import EditPickUpSenderInfo from '../components/pickupReceiver/editPickUpReceiver.jsx'

import RegionStores from '../components/state/regionState.js';

// useStrict(true);
const stores = { RegionStores };

class Container extends React.Component {
    render() {
        if (this.props.children) {
            return (<div>{this.props.children}</div>)
        }else{
            return <Home />
        }
    }
}
const routes = (
    <Provider {...stores}>
      <Router history={hashHistory}>
      <Route path="/" component={Container}>
        <Route path="sendReveicer" component={SendReveicer}/>
        <Route path="sendReveicer/edit/region" component={Region}/>
        <Route path="pickSender" component={PickUpSender}/>
        <Route path="pickReveicer" component={PickReveicer}/>
        <Route path="pickReveicer/edit" component={AddPickUpSenderInfo}/>
        <Route path="pickReveicer/edit/params" component={EditPickUpSenderInfo}/>
      </Route>
      </Router>
    </Provider>
);
module.exports = routes;
