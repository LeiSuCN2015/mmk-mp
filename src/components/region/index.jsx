import React, { Component } from 'react';
import {Link,IndexLink} from 'react-router';
import { observer,inject } from 'mobx-react';
import './region.less'



// @inject("RegionStores") @observer class Region extends Component {
@observer(["RegionStores"]) class Region extends Component {
  constructor(props) {
      super(props);
      // 初始状态
      this.state = {
          //省
          provinces: [],
          //市
          cities: [],
          //区
          areas: [],
          province: false,
          city: false,
          area: false
      };
  }
  componentWillMount() {
    mw.getCity((res)=>{
      this.setCities(res)
   })
  }
  setCities(cities) {
      let state = this.state;
      // 省
      state.provinces = [];
      cities.province.map((p) => {
          state.provinces.push(p);
          if (p.code == '440000') {
              state.province = p;
          }
      });
      // 市
      state.cities = [];
      // 区
      state.areas = [];
      this.setState(state);
  }
      _resetProvince() {
          this.state.area = false;
          this.state.left = "0";
          this.setState(this.state);
      }

      _chooseProvince(p) {
          this.state.province = p;
          this.setState(this.state);
      }

      _chooseCity(c) {
          if (c.area && c.area.length > 0) {
              this.state.city = c;
              this.state.left = "-50%";
              this.setState(this.state);

          } else {
              this.state.city = {code: c.code, name: ''};
              this._chooseArea(c);
          }
      }

      _chooseArea(a) {
          this.state.area = a;
          this.setState(this.state);
          var area = {
              province: {
                  code: this.state.province.code,
                  name: this.state.province.name
              },
              city: {
                  code: this.state.city.code,
                  name: this.state.city.name
              },
              area: {
                  code: this.state.area.code,
                  name: this.state.area.name
              }
          };
          this.props.RegionStores.setRegion(area);
          this.props.history.goBack();
      }


      _goback(){
        this.props.history.goBack();
      }

  render(){
    let self = this;
    let provinceLis = [];
    this.state.provinces.map((p) => {
        let clz = '';
        if (p.code == this.state.province.code) {
            clz = 'actived';
        }
        provinceLis.push(<li className={clz} onClick={()=>{self._chooseProvince(p)}}  key={p.code}>{p.name}</li>)
    });

    let cityLis = [];
    let cities = this.state.province.city ? this.state.province.city : [];
    cities.map((c) => {
        let clz = '';
        if (this.state.city && c.code == this.state.city.code) {
            clz = 'actived';
        }
        cityLis.push(<li className={clz} onClick={ ()=>{self._chooseCity(c)}} key={c.code}>{c.name}</li>)
    });

    let areaLis = [];
    if (this.state.city) {
        let areas = this.state.city.area;
        areas && areas.map((a) => {
            let clz = '';
            if (this.state.area && a.code == this.state.area.code) {
                clz = 'actived';
            }
            areaLis.push(<li className={clz} onClick={()=>{self._chooseArea(a)}} key={a.code}>{a.name}</li>)
        })
    }

    let provinceName = this.state.province ? ' ' + this.state.province.name : '';
    let cityName = this.state.city ? '  >  ' + this.state.city.name : '';
    let areaName = this.state.area ? '  >  ' + this.state.area.name : '';
    let leftMove=this.state.left?this.state.left:'';

    let pListClz = 'region-list ';
    let aListClz = 'region-list ';
    let cListClz = 'region-list ';
    if (this.state.city) {
        cListClz += 'region-left';
    } else {
        pListClz += 'region-left';
    }
    return(
      <div>
        <div data-flex='dir:left box:first' className='area-nav'>
          <span onClick={this._goback.bind(this)}>取消</span>
          <span style={{paddingLeft:'30%'}}>收件地区</span>
        </div>
        <div className="region-nav">
            <a id="area_nav_p" onClick={this._resetProvince.bind(this)}>{provinceName}</a>
            <a id="area_nav_c" data-level='c'>{cityName}</a>
            <a id="area_nav_a" data-level='a'>{areaName}</a>
        </div>
        <div className="region-body">
            <div className="region-body-warp" style={{left:leftMove}}>
                <div className={pListClz}>
                    <ul>{provinceLis}</ul>
                </div>
                <div className={cListClz}>
                    <ul>{cityLis}</ul>
                </div>
                <div className={aListClz}>
                    <ul>{areaLis}</ul>
                </div>
            </div>
        </div>
      </div>
    )
  }
}
export default Region;
