import React, { Component } from 'react';
import {Link,IndexLink} from 'react-router';
import { observer } from 'mobx-react';
import {Cells, CellsTitle, Cell, CellBody,Dialog,Input,CellFooter} from 'react-weui';
import './send.less';

const {Alert} = Dialog;

@observer(["RegionStores"]) class SendSender extends Component {
  constructor(props){
    super(props)
    this.state={
      senderName:'',
      senderPhone:'',
      senderStore:'',
      storeInfo:'',
      alert: {
          title: '请完善寄件人信息',
          buttons: [
              {
                  label: '好的',
                  onClick: this.hideAlert.bind(this)
              }
          ]
      }
    }
  }
  showAlert() {
      this.setState({showAlert: true});
  }

  hideAlert() {
      this.setState({showAlert: false});
  }
  componentWillMount() {
    let getDefault=util.getLocalData('saved-sendSender1');
    if(getDefault){
      this.setState({
        senderName:getDefault.name,
        senderPhone:getDefault.phone,
        senderStore:getDefault.store,
        storeInfo:getDefault.storeInfo
      })
    }
  }

  componentDidMount() {
    if(this.props.RegionStores.storeState.sid){
      let store=this.props.RegionStores.storeState;
      this.setState({senderStore:store.storename})
    }
    if(this.props.RegionStores.senderState){
      let sendInfo=this.props.RegionStores.senderState;
      this.setState({
        senderName:sendInfo.senderName,
        senderPhone:sendInfo.senderPhone
      })
    }
  }

  editSenderInfo(value,e){
    switch (value) {
      case 'senderName':
      let nameValue=e.target.value.replace(/(^\s*)|(\s*$)/g, "");
      this.setState({senderName:nameValue})
      break;
      case 'senderPhone':
      let phoneValue=e.target.value.replace(/(^\s*)|(\s*$)/g, "");
      this.setState({senderPhone:phoneValue})
      break;
    }
  }

  editStore(){
    let params={};
    const data=this.state;
    if(data.senderName){params.senderName=data.senderName};
    if(data.senderPhone){params.senderPhone=data.senderPhone};
    this.props.RegionStores.setSenderState(params);
    this.props.history.push('sendSender/store');
  }

  _saveSender() {
      let sender = {};
      sender.name = this.state.senderName;
      sender.phone = this.state.senderPhone;
      sender.store = this.state.senderStore;
      sender.storeInfo={}
      if(this.props.RegionStores.storeState.sid){
        sender.storeInfo.sid=this.props.RegionStores.storeState.sid;
        sender.storeInfo.storename=this.props.RegionStores.storeState.storename;
        sender.storeInfo.phone=this.props.RegionStores.storeState.phone;
        sender.storeInfo.address=this.props.RegionStores.storeState.address;
      }else{
        sender.storeInfo=this.state.storeInfo;
      }

      //姓名验证
      if (!util.regName.test(sender.name)) {
          this.setState({
              showAlert: true,
              textAlert: "请输入正确的姓名"
          });
          return false;
      }
      //寄件人验证
      if (!sender.phone) {
          this.setState({
              showAlert: true,
              textAlert: "寄件人电话不能为空"
          });
          return false;
      }
      // 门店验证
      if (!sender.store) {
          this.setState({
              showAlert: true,
              textAlert: "请选择一家门店作为代寄点"
          });
          return false;
      }
      util.setLocalData('saved-sendSender1',sender);
      this.props.history.push('/');
  };

  render() {
      return (
        <div>
          <div data-flex='dir:left box:justify' className='area-nav'>
            <IndexLink to={`/`}><span>取消</span></IndexLink>
            <span style={{textAlign:'center'}}>寄件人信息</span>
            <span onClick={this._saveSender.bind(this)}>保存</span>
          </div>
          <Cells>
              <Cell>
                  <CellBody>
                      <Input type="text" placeholder="请输入寄件人姓名" onChange={this.editSenderInfo.bind(this,'senderName')}
                         value={this.state.senderName}/>
                  </CellBody>
              </Cell>
              <Cell>
                  <CellBody>
                      <Input type="text" placeholder="请输入寄件人手机号码" onChange={this.editSenderInfo.bind(this,'senderPhone')}
                        value={this.state.senderPhone}/>
                  </CellBody>
              </Cell>
          </Cells>
          <CellsTitle>请选择寄件点</CellsTitle>
          <Cells access>
              <Cell>
                  <CellBody onClick={this.editStore.bind(this)}>
                      <Input type="text" placeholder="请选择门店" value={this.state.senderStore} disabled/>
                  </CellBody>
                  <CellFooter/>
              </Cell>
          </Cells>
          <Alert
              show={this.state.showAlert}
              title={this.state.alert.title}
              buttons={this.state.alert.buttons}>
              {this.state.textAlert}
          </Alert>
        </div>
      );
  }
}
export default SendSender;
