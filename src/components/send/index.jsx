import React, { Component } from 'react';
import { observer } from 'mobx-react';
import {Cell,Cells,CellHeader,CellBody,CellFooter,Mask,Toast,Form,FormCell,TextArea} from 'react-weui';
import {Link} from 'react-router';
import './send.less';
import nextImg from '../../images/icon_banck.png';
import tip from '../../images/tip.png';

class Send extends Component {
  constructor(props){
    super(props);
    let _receiverData=util.getLocalData('indexReceiver');
    let _senderData=util.getLocalData('saved-sendSender1');
    this.state={
      receiverData:_receiverData,//收件人默认地址
      senderData:_senderData,//寄件人默认地址
      itemType:'文件',//物品类型
      itemTypeWeight : 1000,//物品重量
      isItemTypeShow:false,//是否显示物品类型
      isItemWeightShow:false,//是否显示物品重量
      isExpressMaskShow:false,//是否显示对快递小哥说
      isOtherItem:false,//是否显示物品自填类型
      cost:0,//估算费用
      price:0,//运费
      showLoading:false,
      submit: false,//是否可以提交
      note:''//对快递员说
    }
  }
  componentWillMount() {
    let _receiverDatas=util.getLocalData('saved-receivers');
    let _senderData=util.getLocalData('saved-sendSender1');
    let _itemType=util.getLocalData('set-type');
    let _itemTypeWeight=util.getLocalData('set-weight');
    if(_receiverDatas){
      if(_receiverDatas.length==0){
        this.state.receiverData=null;
      }
    }
    if(_senderData){
      if(_senderData.length==0){
        this.state.senderData=null;
      }
    }
    if(_itemType){
      this.state.itemType=_itemType;
    }
    if(_itemTypeWeight){
      this.state.itemTypeWeight=_itemTypeWeight;
    }
    this.setState(this.state)
  }
  componentDidMount() {
    //this.computeCost();
    this.buildOrder();
  }

  //显示物品
  showItemsType(){
    this.setState({isItemTypeShow:true})
  }
  hideItemsType(){
    this.setState({isItemTypeShow:false})
  }
  //关闭重量显示
  hideItemWeight(){
    this.setState({isItemWeightShow:false})
  }
  //设置物品类型
  setType(type) {
    util.setLocalData('set-type',type);
    this.setState({
        itemType   : type,
        isItemTypeShow: false,
        isItemWeightShow: true
    });
  }
  //重新选择物品类型
  showType() {
      this.setState({
        isItemTypeShow: true,
        isItemWeightShow : false
      })
  }
  //显示其它物品类型
  otherItem(){
    this.setState({isOtherItem:true})
  }
  //设置物品重量
  setWeight(weight) {
      util.setLocalData('set-weight',weight);
      this.setState({
          itemTypeWeight: weight,
          isItemWeightShow : false
      });
      //this.computeCost(weight);
      this.buildOrder();
  }

  //显示对快递小哥说
  showExpressMask(){
    this.setState({isExpressMaskShow:true})
  }
  hideExpressMask(){
    this.setState({isExpressMaskShow:false})
  }

  //计算运费
  computeCost(itemTypeWeight=this.state.itemTypeWeight){
    const {receiverData}=this.state;
    if(itemTypeWeight > 0 && receiverData.receiver.area){
      this.setState({showLoading:true})
      let params = {};
      params.targetAreaCode=receiverData.receiver.area.code;
      params.expressWeight=itemTypeWeight;
      mw.getComputeValuation(params,(p)=>{
        this.state.cost=p.cost;
        this.state.price=p.price;
        this.state.showLoading=false;
        this.setState(this.state)
      })
    }
  }

  //对快递员说
  setNote(e){
    this.setState({note:e.target.value})
  }
  //备注
  noteButton(value){
    let noteValue =this.state.note;
    if(0==value){
      if(noteValue.indexOf("请带包装袋")>-1){
        this.state.note=noteValue.replace(/请带包装袋/g, "请带纸箱");
      }else if(noteValue.indexOf("请带纸箱")>-1){
        return ;
      }else{
        this.state.note="请带纸箱"+noteValue;
      }
    }else{
      if(noteValue.indexOf("请带纸箱")>-1){
        this.state.note=noteValue.replace(/请带纸箱/g, "请带包装袋");
      }else if(noteValue.indexOf("请带包装袋")>-1){
        return ;
      }else{
        this.state.note="请带包装袋"+noteValue;
      }
    }
    this.setState({note:this.state.note})
  }
  //检测是否可以提交
  buildOrder() {
    let sender = this.state.senderData;
    let receiver = this.state.receiverData;
    let itemType = this.state.itemType;
    let itemTypeWeight = this.state.itemTypeWeight;
    if (sender && receiver.receiver.area && itemType && itemTypeWeight) {
          this.setState({submit:true});
        }
    }
  //提交订单
  submit(){
    if(this.state.submit){
      let params = {};
      const paramsData=this.state
      params.expressType=paramsData.itemType;
      params.expressWeight=paramsData.itemTypeWeight;
      params.senderName=paramsData.senderData.name;
      params.senderPhone=paramsData.senderData.phone;
      params.senderAddress=paramsData.senderData.storeInfo.address;
      params.targetName=paramsData.receiverData.name;
      params.targetPhone=paramsData.receiverData.phone;
      params.targetAddress=paramsData.receiverData.receiverArea+paramsData.receiverData.address;
      params.targetAreaCode=paramsData.receiverData.receiver.area.code;
      params.expressMark=this.state.note;//对快递员说
      params.eid=0;
      params.sid=paramsData.senderData.storeInfo.sid;
      this.setState({showLoading:true});
      mw.toSubmitOrder(params);
      util.removeLocalData('set-type')
      util.removeLocalData('set-weight')
    }
  }

  render(){
    const {receiverData,senderData}=this.state
    let receiverName='';
    let receiverPhone='';
    let receiverArea='请填写收件地址';
    let receiverAddress='';
    if(receiverData){
      receiverName=receiverData.name;
      receiverPhone=receiverData.phone;
      receiverArea=receiverData.receiverArea;
      receiverAddress=receiverData.address;
    }
    let senderName='';
    let senderPhone='';
    let senderArea='请填写寄件地址';
    let senderAddress='';
    if(senderData){
      senderName=senderData.name;
      senderPhone=senderData.phone;
      senderArea=senderData.storeInfo.storename;
      senderAddress=senderData.storeInfo.address;
    }

    let isItemType;
    if(this.state.isItemTypeShow){
      isItemType='itemType-show';
    }else{
      isItemType='itemType-hide';
    }

    let showOtherItem;
    if(this.state.isOtherItem){
      showOtherItem='itemType-show';
    }else{
      showOtherItem='itemType-hide';
    }

    let isItemWeight;
    if(this.state.isItemWeightShow){
      isItemWeight='itemType-show';
    }else{
      isItemWeight='itemType-hide';
    }
    let isExpressMask;
    if(this.state.isExpressMaskShow){
      isExpressMask='itemType-show';
    }else{
      isExpressMask='itemType-hide';
    }
    let commit;
    if (!this.state.submit) {
        commit = "disabled";
    }else{
      commit='btnsubmit';
    }
    return (
      <div>
        <Link to={`sendSender`}>
          <div data-flex='dir:left box:justify' className='info'>
            <div data-flex='cross:center'>
              <div className='sender-icon'>
                <label>寄</label>
              </div>
            </div>
            <div className='person-info'>
              <p>{senderName} {senderPhone}</p>
              <p>{senderArea}</p>
              <p>{senderAddress}</p>
            </div>
            <div data-flex='cross:center'>
              <img src={nextImg}/>
            </div>
          </div>
        </Link>
        <Link to={`sendReveicer`}>
          <div data-flex='dir:left box:justify' className='info info-receiver'>
            <div data-flex='cross:center'>
              <div className='receiver-icon'>
                <label>收</label>
              </div>
            </div>
            <div className='person-info'>
              <p>{receiverName} {receiverPhone}</p>
              <p>{receiverArea}</p>
              <p>{receiverAddress}</p>
            </div>
            <div data-flex='cross:center'>
              <img src={nextImg}/>
            </div>
          </div>
        </Link>
        <Cells access style={{marginTop:'0.5rem'}}>
          <Cell onClick={this.showItemsType.bind(this)}>
            <CellBody>
              物品信息
            </CellBody>
            <CellFooter>
              {this.state.itemType} {this.state.itemTypeWeight > 0 ? ( this.state.itemTypeWeight / 1000).toFixed(1) + 'KG' : ''}
            </CellFooter>
          </Cell>
          <Cell onClick={this.showExpressMask.bind(this)}>
            <CellHeader>
              <img src={tip} className='tip-icon'/>
            </CellHeader>
            <CellBody>
              对快递小哥说
            </CellBody>
            <CellFooter>
              {this.state.note.length>6? this.state.note.substr(0,5)+"..." :this.state.note}
            </CellFooter>
          </Cell>
        </Cells>
        <Cells style={{marginTop:'0.5rem'}}>
          <Cell>
            <CellBody>
              运费
            </CellBody>
            <CellFooter className='sum'>
              ¥{(this.state.price/100).toFixed(2)}
            </CellFooter>
          </Cell>
          <Cell>
            <CellBody>
              优惠券
            </CellBody>
            <CellFooter className='sum'>
              ¥{((this.state.price-this.state.cost)/100).toFixed(2)}
            </CellFooter>
          </Cell>
        </Cells>
        <div data-flex='box:last' className='order-submit'>
          <div>
            <span style={{display:'block'}}>估算运费 <span>¥{(this.state.cost/100).toFixed(2)}</span></span>
            <span style={{display:'block'}}>我同意<Link to={`agreement`}>《快件运单契约条款》</Link></span>
          </div>
          <div data-flex='main:center cross:center'>
            <button className={commit} onClick={this.submit.bind(this)}>提交</button>
          </div>
        </div>

        <div className={isItemType}>
          <Mask onClick={this.hideItemsType.bind(this)}/>
            <div className="weui_actionsheet weui_actionsheet_toggle">
                <div className="actionsheet_buttons">
                    <div className="itemType-title">
                        <span>物品名称</span>
                    </div>
                    <div className="row">
                        <div className="col button-col-padding">
                            <a className="weui_btn weui_btn_default"
                              onClick={ this.setType.bind(this,"文件")}>文件</a>
                        </div>
                        <div className="col button-col-padding">
                            <a className="weui_btn weui_btn_default"
                              onClick={ this.setType.bind(this,"数码产品")}>数码产品</a>
                        </div>
                        <div className="col button-col-padding">
                            <a className="weui_btn weui_btn_default"
                              onClick={ this.setType.bind(this,"日用品")}>日用品</a>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col button-col-padding">
                            <a className="weui_btn weui_btn_default"
                              onClick={ this.setType.bind(this,"服饰")}>服饰</a>
                        </div>
                        <div className="col button-col-padding">
                            <a className="weui_btn weui_btn_default"
                              onClick={ this.setType.bind(this,"食品")}>食品</a>
                        </div>
                        <div className="col button-col-padding">
                            <a className="weui_btn weui_btn_default"
                              onClick={this.otherItem.bind(this)}>其他</a>
                        </div>
                    </div>
                </div>
                <div className={"actionsheet-custom "+showOtherItem}>
                    <input type="text" placeholder="请输入物品类型" ref="otherItemsType"/>
                    <a onClick={()=>{this.setType(this.refs.otherItemsType.value)}}>确定</a>
                </div>
            </div>
        </div>

        <div className={isItemWeight}>
          <Mask onClick={this.hideItemWeight.bind(this)}/>
            <div className="weui_actionsheet weui_actionsheet_toggle">
                <div className="actionsheet_buttons">
                  <div className="weui_cells weui_cells_access" style={{marginTop:"0px"}} onClick={()=>{this.showType()}}>
                    <span className="weui_cell itemWeight-title">
                      <div className="weui_cell_bd weui_cell_primary">
                        <span>物品名称</span>
                        <span style={{float:"right"}}>{this.state.itemType}</span>
                      </div>
                      <div className="weui_cell_ft">
                      </div>
                    </span>
                  </div>
                    <div className="itemType-title itemWeight-span">
                        <span>物品重量</span>
                    </div>
                    <div className="row">
                        <div className="col button-col-padding ">
                            <a className="weui_btn weui_btn_default"
                               onClick={()=> this.setWeight(1000)}>1kg</a>
                        </div>
                        <div className="col button-col-padding ">
                            <a className="weui_btn weui_btn_default"
                               onClick={()=> this.setWeight(2000)}>2kg</a>
                        </div>
                        <div className="col button-col-padding ">
                            <a className="weui_btn weui_btn_default"
                               onClick={()=> this.setWeight(3000)}>3kg</a>
                        </div>
                    </div>
                    <div className="row">
                      <div className="col button-col-padding ">
                          <a className="weui_btn weui_btn_default"
                             onClick={()=>{this.setWeight(4000)}}>4kg</a>
                      </div>
                      <div className="col button-col-padding ">
                          <a className="weui_btn weui_btn_default"
                             onClick={()=>{this.setWeight(5000)}}>5kg</a>
                      </div>
                      <div className="col button-col-padding ">
                          <a className="weui_btn weui_btn_default">其他</a>
                      </div>
                    </div>
                </div>
                <div className={"actionsheet-custom "+showOtherItem}>
                  <input type="number" placeholder="请输入重量" ref="itemsType"/>
                  <a onClick={()=>{this.setWeight(this.refs.itemsType.value*1000)}}>确定</a>
                </div>
            </div>
        </div>

        <div className={isExpressMask}>
            <Mask onClick={this.hideExpressMask.bind(this)}/>
            <div className="weui_actionsheet weui_actionsheet_toggle">
              <div className='expor-sad'>
                <div data-flex='main:right'>
                  <span style={{color:"#fa982e"}} onClick={this.hideExpressMask.bind(this)}>确定</span>
                </div>
                <Form>
                  <FormCell>
                    <CellBody>
                      <TextArea onChange={this.setNote.bind(this)} value={this.state.note}
                        placeholder="请选择下面内容或者输入其他内容" rows="2" maxlength="30"></TextArea>
                    </CellBody>
                  </FormCell>
                </Form>
                <div>
                  根据您的需要选择内容
                  <div className='expor-sad-button' data-flex='box:mean'>
                    <button onClick={this.noteButton.bind(this,0)}>请带纸箱</button>
                    <button onClick={this.noteButton.bind(this,1)}>请带包装袋</button>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <Toast icon="loading" show={this.state.showLoading}>
          加载中...
        </Toast>
      </div>
    )
  }
};

export default Send;
