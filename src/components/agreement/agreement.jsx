import React, { Component } from 'react';
import {IndexLink} from 'react-router';

export default class Agreement extends Component {
    render() {
        return (
            <div>
              <div data-flex='dir:left box:first' className='area-nav' style={{position:'fixed',left:0,right:0}}>
                <IndexLink to={`/`}><span>取消</span></IndexLink>
                <span style={{paddingLeft:'37%'}}>条款</span>
              </div>
                <div style={{padding: '1em',paddingTop:'5rem',fontSize:'1rem'}}>
                    <p>1、承运人：为中国国内各城市获得上海韵达货运有限公司商标使用权许可的韵达快运代理商。韵达快运代理商自负盈亏，独立承担经济与法律责任。</p>
                    <p>2、收件人应及时提货，超期加收暂存费，自运单签发之日起超过30天的，将退回发件方，退回运费及暂存费由发件人承担。</p>
                    <p>3、收货人提货时须当面验收货物的件数，事后概不负责。</p>
                    <p>
                        4、双方责任：
                        <span>（1）货物的包装由发件人负责，包装应满足运输、装卸要求。</span><br/>
                        <span>（2）承运人只按件数接收已包装好的货物，不对包装内的货物名称、数量或内在质量承担任何责任。</span><br/>
                        <span>（3）发件人保证托运货物不属于以下物品：危险品或易碎、液态的物品；信函和其它具有信函性质的物品；国家规定禁运的物品；有价证券、增值税发票、货币、手机等高价值贵重物品。</span><br/>
                        <span>(4）对收货人付费的货件，收件人如不当场支付运费，则承运人有权置留该货件，责任由发件人承担。</span>
                    </p>
                    <p>5、保价：重要物品务必保价，发件人保价的，在支付正常运费的基础上，应按保价金额的3%支付保价费，保价金额最高为20000元，保价费应在办理物品交寄手续时当场支付，未支付保价费的视为未保价。</p>
                    <p>
                        6、赔偿：
                        <span>（1）任何索赔应在物品交寄后30天内（以揽件人签收日期为准），由发件人以书面形式提出，同时必须出具本单收件人联原件及运费支付凭证，具体以行业标准为准。</span><br/>
                        <span>（2）寄件人确认每票未保价的托寄物品价值不高于5000元。</span><br/>
                        <span>（3）保价的物品，按实际损失，在本契约第五条中约定的范围赔偿。</span>
                    </p>
                    <p>7、如发生发下事项，承运人不承担任何赔偿责任：
                        <span>（1）因不可抗力因素造成的物品损毁、遗失。</span><br/>
                        <span>（2）因发件人填写的收件地址错误，字迹潦草无法确认或收件人地址变更等导致物品无法送达的。</span><br/>
                        <span>(3）发件人交寄的物品违反本契约第3条规定的。</span>
                    </p>
                    <p>8、留置权，如发生拖欠运费及相关费用，承运人有权留置承运的物品，直到发件人提供相应担保或付清款止，由此产生的损失由发件人承担。</p>
                    <p>9、运输途中如有关部门对货物有异议被扣留或罚款均由发件人负责处理。并提供有关有效证件，并承担由此给承运人带来的经济损失。</p>
                    <p>10、查询：本运单签发起30日内办理查询，逾期责任由发件人承担。</p>
                    <p>11、发件人和承运人可以另订补充协议对本契约进行补充、修改。</p>
                    <p>12、本面单适用于许可享有韵达注册商标使用权的代理商</p>
                </div>
            </div>
        );
    }
}
