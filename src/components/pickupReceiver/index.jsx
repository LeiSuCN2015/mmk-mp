import React, { Component } from 'react';
import {Link,IndexLink} from 'react-router';
import { observer } from 'mobx-react';
import {Button} from 'react-weui';
import defaultArea from '../../images/icon_xuanzedizhi.png';

class AreaListItem extends Component {
  constructor(props){
    super(props)
  }

  _chooseMe(){
    this.props.onClick(this.props.receiver);
  }

  _openEditor(e) {
      e.stopPropagation();
      let params=this.props.receiver;
      params.index=this.props.index;
      this.props.RegionStores.setEditReceiver(params);
      this.props.history.push('pickReveicer/edit/params');
  }

  render(){
    const receiver=this.props.receiver;
    return(
      <div data-flex='dir:left box:justify' className={this.props.className ? this.props.className :''}
        onClick={this._chooseMe.bind(this)}>
        <div className='area-img' style={{width:'2rem'}}>
          <img src={defaultArea} className={this.props.isCheck ?'show-img':''}/>
        </div>
        <div className='person-info' style={{lineHeight:'1.5rem'}}>
          <p>{receiver.name} {receiver.phone}</p>
          <p>{receiver.receiverArea}</p>
          <p>{receiver.address}</p>
        </div>
        <div data-flex='cross:center' onClick={this._openEditor.bind(this)}>
          <span className='area-edit'>编辑</span>
        </div>
      </div>
    )
  }
}

@observer(["RegionStores"])  class AreaList extends Component{

  constructor(props){
    super(props)
      this.state = {
          receivers: "",
          idx:null
      }
  }

  componentWillMount() {
      this.loadSavedReceivers();
  }

  loadSavedReceivers() {
      let savedReceivers = util.getLocalData("saved-pickupReceivers");
      let defaultReceiver = util.getLocalData("inxPickUpReceiver");
      let defaultIdx=-1;
      if(savedReceivers){
        savedReceivers.map((s,index) =>{
          let isName= (s.name==defaultReceiver.name);
          let isPhone= (s.phone==defaultReceiver.phone);
          let isAddress= (s.address==defaultReceiver.address);
          let isReceiverArea= (s.receiverArea==defaultReceiver.receiverArea);
          if( isName && isPhone && isAddress && isReceiverArea){
            defaultIdx=index;
          }
        })
      }
      if(defaultIdx!=-1){
        this.setState({idx:defaultIdx})
      }
      this.setState({receivers: savedReceivers ? savedReceivers : []})
  }

  chooseMe(receiver){
    util.setLocalData("inxPickUpReceiver",receiver);
    this.props.history.push('/');
  }
  render(){
    let list = [];
    if (this.state.receivers) {
        this.state.receivers.map((receiver,idx) =>{
          let isCheck=false;
          if(this.state.idx==idx){
            isCheck=true;
          }
          list.push(<AreaListItem className='area-body-item' {...this.props}
           onClick={this.chooseMe.bind(this)} index={idx}
           key={idx} isCheck={isCheck} receiver={receiver}/>)
        }
      )
    }
    return(
      <div>
        <div data-flex='dir:left box:first' className='area-nav'>
          <IndexLink to={`/`}><span>取消</span></IndexLink>
          <span style={{paddingLeft:'25%'}}>选择收件人信息</span>
        </div>
        <div className='area-body'>
          {list}
        </div>
        <Link to={`pickReveicer/edit`}>
          <div className='area-footer'>
             <Button type="primary">+新增收件人信息</Button>
          </div>
        </Link>
      </div>
    )
  }
}

export default AreaList;
