var mw={};
// var SERVER='';
var SERVER='http://wxtyj.kuaididaola.com';
(function() {
  function status(response) {
      if (response.status >= 200 && response.status < 300) {
          return Promise.resolve(response)
      } else {
          return Promise.reject(new Error(response.statusText))
      }
  }
  //获取省市区
  mw.getCity=function(succCb){
    axios.get('http://static.mailworld.org/cities.json').then(status).then(function (res){
      var data =res.data;
      succCb(data);
    })
  }
  //获取门店列表
  mw.getStoreList=function(succCb){
    axios.post(SERVER + '/store/getall').then(status).then(function (res){
      if (res.data.code == '10000') {
          succCb(res.data.data);
      }
    })
  }
  //获取快递员列表
  mw.getExporList=function(succCb){
    axios.post(SERVER + '/expor/getlist').then(status).then(function (res){
      if (res.data.code == '10000') {
          succCb(res.data.data);
      }
    })
  }
  //计算运费
  mw.getComputeValuation=function(params,succCb){
    axios.post(SERVER + '/order/getprice',params).then(status).then(function (res){
      if (res.data.code == '10000') {
          succCb(res.data.data);
      }
    })
  }
  //提交订单
  mw.toSubmitOrder=function(params){
    axios.post(SERVER + '/order/build',params).then(status).then(function (res){
      console.log(res.data);
      if (res.data.code == '10000') {
        window.location = SERVER + '/my/orderDetail?id=' + res.data.oid
      }
    })
  }


})();
