var url = window.location.search;
var id = url.substring(url.lastIndexOf('=') + 1, url.length);
var order = {};
var show = true;
var price = '';
var SERVER='http://wxtyj.kuaididaola.com'
//初始化
function init(){
  $('#loading').show();
  $('.main').hide();
}

init();

//获取订单详情
function getOrder(id, res) {
  var params = {
    oid: id
  }
  $.post(window.SERVER + '/order/getinfo', params, function (resp) {
    if (resp.code == '10000') {
      res(resp.data);
    }
  });
}

getOrder(id, function (data) {
  $('#orderId').html(data.oid);
  // 快递单号
  if( data.epid && data.epid != 0 ){
    $('#expressId').html(data.epid);
  }
  // 下单时间：
  var orderTime = new Date(parseInt(data.createDate, 0) * 1000);
  $('#orderTime').html(orderTime.toLocaleDateString() + ' ' + orderTime.toLocaleTimeString());
  // 快递小哥：
  if( data.expor && data.expor.eid )
    $('#expor').html(data.expor.nickname + ' <a href="tel:' + data.expor.phone + '">' + data.expor.phone + '</a>');
  $('#senderName').html(data.senderName);
  $('#senderPhone').html(data.senderPhone);
  $('#senderAddress').html(data.senderAddress);
  $('#receiverName').html(data.targetName);
  $('#receiverPhone').html(data.targetPhone);
  $('#receiverAddress').html(data.targetAddress);
  $('#itemType').html(data.expressType);
  var expressKg=(data.expressWeight/1000).toFixed(2)+'kg';
  $('#expressKg').html(expressKg);
  $('#submit').hide();
  $('#loading').hide();
  $('.main').show();
});
